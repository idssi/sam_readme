# SAM_README

## Manuale & Relazione
Scaricare la repo: `git clone https://bitbucket.org/idssi/sam_readme.git` per avere tutti i file di relazione e manuale di utilizzo.

Consultare il [manuale d'uso](https://bitbucket.org/idssi/sam_readme/src/master/Relazione%20%26%20Manuale/Manuale%20d'uso.pdf) prima di utilizzare il software.


## Installazione & Inizializzazione

### Progetto

Di seguito sono riportati dei passi fondamentali da seguire per poter far partire l'intero porgetto.

Si consiglia di avere sempre con se il [manuale di utilizzo](https://bitbucket.org/idssi/sam_readme/src/master/Relazione%20%26%20Manuale/Manuale%20d'uso.pdf) in modo da avere maggiori dettagli sulle tecnologie, l'installazione e l'utilizzo effettivo del software.

1. Scaricare le varie Repo presenti nella home della repository di progetto: https://bitbucket.org/dashboard/overview

	 - Frontend (Angular - SAM) : `git clone https://bitbucket.org/idssi/sam.git` 


	 - Backend (Express - SAM-API) : `git clone https://bitbucket.org/idssi/sam-api.git` 


	 - Blockchain (Quorum - SAM-Quorum) : `git clone https://bitbucket.org/idssi/sam-quorum.git` 


	 - SmartContract (Truffle - SAM-Contract) : `git clone https://bitbucket.org/idssi/sam_contracts.git` 
 
 
2. Copiare le varie repo scaricate in una directory comune sul proprio PC:
	 - Esempio: SAM-Project (Directory principale)


3. Avviare il terminale e posizionarsi nella directory di progetto
	 - `cd percorso/SAM-Project`

4. Installazione delle tecnologie utilizzate:
	 - Installare [Virtual Box](https://www.virtualbox.org/)
	 - Installare [Vagrant](https://www.vagrantup.com/downloads.html)
	 - Installare [NodeJs](https://nodejs.org/it/)
	 - Installare [Truffle](https://www.trufflesuite.com/truffle)

5. Inizializzare le varie tecnologie posizionandosi, da terminale, su ogni singola directory di progetto e digitare i comandi in ordine di elenco:

	  - sam-quorum: 
		- `vagrant up` 
		- `vagrant ssh`
		- `cd quorum-examples/7nodes`
		- `./raft-init.sh`
		- `./raft-start.sh`
	  - SAM_Contracts: 
		- `truffle migrate`
	  - SAM: 
		- `npm install`
		- `npm start`
	  - SAM-API: 
		- `npm install`
		- `npm start`
	
6. Avviare la Web App: 
	  - Browser: http://localhost:4200 (Si consiglia l'utilizzo di Chrome)
	  

7. Credenziali di default per l'accesso:
	 - Direttore dei lavori(DL): 
	 	- Mail: direttore@direttore.it
		- Password: Direttore123
	 - RUP:
	 	- Mail: rup@rup.it
		- Password: Rup12345
	 - Impresa: 
	 	- Mail: impresa@impresa.it
		- Paswword: Impresa123
		
8. Si consiglia di installare sul proprio smartphone [questo software](https://auth0.com/multifactor-authentication) per l'autenticazione a doppio fattore


9. Buon divertimento!

10. Per qualsiasi problema o informazione:
	 - pietro_rignanese@hotmail.it
	 - poly94.ap@gmail.com
	 - boccifpb@hotmail.it
 



